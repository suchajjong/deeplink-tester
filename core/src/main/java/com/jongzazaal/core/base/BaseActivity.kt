package com.jongzazaal.core.base

import android.graphics.Insets
import android.os.Build
import android.os.Bundle
import android.util.DisplayMetrics
import android.util.Log
import android.view.WindowInsets
import androidx.appcompat.app.AppCompatActivity
import com.google.android.gms.ads.AdError
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.FullScreenContentCallback
import com.google.android.gms.ads.LoadAdError
import com.google.android.gms.ads.interstitial.InterstitialAd
import com.google.android.gms.ads.interstitial.InterstitialAdLoadCallback
import com.jongzazaal.core.R
import com.jongzazaal.core.extension.showToast
import com.jongzazaal.core.extension.showToastDebug
import com.jongzazaal.core.utility.ScreenUtility


open class BaseActivity: AppCompatActivity() {
    private lateinit var display: DisplayMetrics
    private var LOG_TAG_ADS = "tag_ad"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        display = ScreenUtility.getDisplay(this)
        ScreenUtility.setDisplay(display)
    }

    open fun getScreenWidth(): Int {
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            val windowMetrics = windowManager.currentWindowMetrics
            val insets: Insets = windowMetrics.windowInsets
                .getInsetsIgnoringVisibility(WindowInsets.Type.systemBars())
            windowMetrics.bounds.width() - insets.left - insets.right
        } else {
            val displayMetrics = DisplayMetrics()
            windowManager.defaultDisplay.getMetrics(displayMetrics)
            displayMetrics.widthPixels
        }
    }

    private var interstitialAd: InterstitialAd? = null
    private var adIsLoading: Boolean = false
    private var unitIDInterstitial: String? = null
    fun loadAd(unitID: String) {
        unitIDInterstitial = unitID
        if(interstitialAd != null){return}
        var adRequest = AdRequest.Builder().build()

        InterstitialAd.load(
            this,
            unitIDInterstitial?:"",
            adRequest,
            object : InterstitialAdLoadCallback() {
                override fun onAdFailedToLoad(adError: LoadAdError) {
                    adError.code
                    Log.d(LOG_TAG_ADS, adError.message)
                    interstitialAd = null
                    adIsLoading = false
                    val error = "domain: ${adError.domain}, code: ${adError.code}, " + "message: ${adError.message}"
                    "onAdFailedToLoad() with error $error".showToastDebug()
                }

                override fun onAdLoaded(ad: InterstitialAd) {
                    Log.d(LOG_TAG_ADS, "Ad was loaded.")
                    interstitialAd = ad
                    adIsLoading = false
                    "onAdLoaded()".showToastDebug()
                }
            }
        )
    }

    //    private var eventWinListener: (() -> Unit)? = null
//    fun setOnWinListener(listener: () -> Unit) {
//        eventWinListener = listener
//    }
    // Show the ad if it's ready. Otherwise toast and restart the game.
    fun showInterstitial(listenerComplete: () -> Unit) {
        if (interstitialAd != null) {
            interstitialAd?.fullScreenContentCallback =
                object : FullScreenContentCallback() {
                    override fun onAdDismissedFullScreenContent() {
                        Log.d(LOG_TAG_ADS, "Ad was dismissed.")
                        "Ad was dismissed.".showToastDebug()
                        // Don't forget to set the ad reference to null so you
                        // don't show the ad a second time.
                        interstitialAd = null
                        loadAd(unitIDInterstitial?:"")
                        listenerComplete.invoke()
                    }

                    override fun onAdFailedToShowFullScreenContent(adError: AdError) {
                        Log.d(LOG_TAG_ADS, "Ad failed to show.")
                        "Ad failed to show.".showToastDebug()
                        // Don't forget to set the ad reference to null so you
                        // don't show the ad a second time.
                        interstitialAd = null
                    }

                    override fun onAdShowedFullScreenContent() {
                        Log.d(LOG_TAG_ADS, "Ad showed fullscreen content.")
                        "Ad showed fullscreen content.".showToastDebug()
                        // Called when ad is dismissed.
                    }
                }
            interstitialAd?.show(this)
        } else {
            "Ad wasn't loaded.".showToastDebug()
            listenerComplete.invoke()
        }
    }
}
package com.jongzazaal.core.utility

import android.view.View
import android.view.ViewGroup
import com.google.android.gms.ads.AdSize

//
//  AdMobUtility.kt
//
//  DeepLink Tester
//
//  Created by Jong on 28/12/2022 AD.
//
object AdMobUtility {
    fun getAdSizeAdaptiveBanner(viewGroup: ViewGroup): AdSize{
        var adWidthPixels = viewGroup.width.toFloat()

//             If the ad hasn't been laid out, default to the full screen width.
        if (adWidthPixels == 0f) {
            adWidthPixels = ScreenUtility.getDisplay().widthPixels.toFloat()
        }

        val density = viewGroup.resources.displayMetrics.density
        val adWidth = (adWidthPixels / density).toInt()

        return AdSize.getCurrentOrientationAnchoredAdaptiveBannerAdSize(viewGroup.context, adWidth)
    }

    fun getAdSizeInlineBanner(viewGroup: ViewGroup): AdSize{
        var adWidthPixels = viewGroup.width.toFloat()

//             If the ad hasn't been laid out, default to the full screen width.
        if (adWidthPixels == 0f) {
            adWidthPixels = ScreenUtility.getDisplay().widthPixels.toFloat()
        }

        val density = viewGroup.resources.displayMetrics.density
        val adWidth = (adWidthPixels / density).toInt()

        return AdSize.getCurrentOrientationInlineAdaptiveBannerAdSize(viewGroup.context, adWidth)
    }
}
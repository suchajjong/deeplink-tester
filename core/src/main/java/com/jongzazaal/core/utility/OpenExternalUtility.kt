package com.jongzazaal.core.utility

import android.content.Context
import android.content.Intent
import android.net.Uri
import androidx.browser.customtabs.CustomTabsIntent
import com.jongzazaal.core.extension.showToast


object OpenExternalUtility {
    fun openDeepLink(context: Context, deepLink: String){
        try {
            context.startActivity(
                Intent(Intent.ACTION_VIEW).apply {
                    data = Uri.parse(deepLink)
                }
            )
        } catch (exception: Exception) {
            exception.localizedMessage.showToast()
        }
    }
    fun openChromeCustomTab(context: Context, url: String){
        val builder = CustomTabsIntent.Builder()
        val customTabsIntent = builder.build()
        customTabsIntent.launchUrl(context, Uri.parse(url))
    }
}
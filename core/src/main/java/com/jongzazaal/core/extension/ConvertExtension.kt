package com.jongzazaal.core.extension

import android.content.res.TypedArray
import android.util.DisplayMetrics
import com.jongzazaal.core.DI.KoinContext
import com.jongzazaal.deeplinktester.di.getKoinInstance
import kotlin.math.roundToInt

/**
 * PX To DP
 */
private fun Float.dp(): Float {
    val context = getKoinInstance<KoinContext>().context
    return this / (context.resources.displayMetrics.densityDpi.toFloat() / DisplayMetrics.DENSITY_DEFAULT)
}

private fun Float.px(): Float {
    val context = getKoinInstance<KoinContext>().context
    return this * (context.resources.displayMetrics.densityDpi.toFloat() / DisplayMetrics.DENSITY_DEFAULT)
}

private fun Int.dp(): Int {
    val context = getKoinInstance<KoinContext>().context
    return (this / (context.resources.displayMetrics.densityDpi.toFloat() / DisplayMetrics.DENSITY_DEFAULT)).roundToInt()
}

private fun Int.px(): Int {
    val context = getKoinInstance<KoinContext>().context
    return (this * (context.resources.displayMetrics.densityDpi.toFloat() / DisplayMetrics.DENSITY_DEFAULT)).roundToInt()
}

private fun Int.sp(): Int {
    val context = getKoinInstance<KoinContext>().context
    return (this / (context.resources.displayMetrics.scaledDensity.toFloat())).roundToInt()
}

val Float.dp: Float
    get() = this.px()

val Float.px: Float
    get() = this.dp()

val Int.dp: Int
    get() = this.px()

val Int.px: Int
    get() = this.dp()

val Int.sp: Int
    get() = this.sp()

fun TypedArray.toListResource(): MutableList<Int>{
    val mList = mutableListOf<Int>()
    for (i in (0 until this.length())){
        mList.add(this.getResourceId(i, 0))
    }
    return mList
}
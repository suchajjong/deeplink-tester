package com.jongzazaal.core.extension

import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.widget.Toast
import com.jongzazaal.core.BuildConfig
import com.jongzazaal.core.DI.KoinContext
import com.jongzazaal.core.R
import com.jongzazaal.deeplinktester.di.getKoinInstance

fun Any.showToast() {
    val koinContext: KoinContext = getKoinInstance()
    Toast.makeText(koinContext.context, "${this}", Toast.LENGTH_SHORT).show()
}

fun Any.showToastDebug() {
    if (BuildConfig.IS_PRODUCTION){

    }
    else{
        val context: Context = getKoinInstance()
        Toast.makeText(context, "${this}(debug)", Toast.LENGTH_SHORT).show()
    }
}
fun String.copyClipboard(context: Context){
    val clipboard = context.getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
    val clip = ClipData.newPlainText("Copied Text", this)
    clipboard.setPrimaryClip(clip)
    "copy".showToast()
}
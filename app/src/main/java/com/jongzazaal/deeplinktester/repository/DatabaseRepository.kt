package com.jongzazaal.deeplinktester.repository

import androidx.lifecycle.MutableLiveData
import com.jongzazaal.deeplinktester.manager.database.DatabaseManager
import com.jongzazaal.deeplinktester.manager.database.entity.HistoryEntity
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class DatabaseRepository {
    var historyAll: MutableLiveData<List<HistoryEntity>> = MutableLiveData()
    var databaseInsetSuccess: MutableLiveData<Boolean> = MutableLiveData()
    var databaseRemoveSuccess: MutableLiveData<Boolean> = MutableLiveData()

    suspend fun setReferCode(deepLink: String) {
        withContext(Dispatchers.IO) {
            val model = DatabaseManager.setHistory(deepLink)
            databaseInsetSuccess.postValue(true)
        }
    }

    suspend fun getAllHistory() {
        withContext(Dispatchers.IO) {
            val model = DatabaseManager.getAllHistory()
            historyAll.postValue(model?: arrayListOf())

        }
    }

    suspend fun removeDatabase() {
        withContext(Dispatchers.IO) {
            DatabaseManager.removeDatabase()
            databaseRemoveSuccess.postValue(true)
        }
    }

    suspend fun removeHistory(id: Int) {
        withContext(Dispatchers.IO) {
            DatabaseManager.removeHistory(id)
            databaseRemoveSuccess.postValue(true)
        }
    }
}
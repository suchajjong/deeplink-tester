package com.jongzazaal.deeplinktester.viewmodel

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.jongzazaal.deeplinktester.manager.database.entity.HistoryEntity
import com.jongzazaal.deeplinktester.repository.DatabaseRepository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.launch

class DatabaseViewModel (app: Application): ViewModel() {
    /**
     * Factory for constructing DevByteViewModel with parameter
     */
    class Factory(val app: Application) : ViewModelProvider.Factory {
        override fun <T : ViewModel> create(modelClass: Class<T>): T {
            if (modelClass.isAssignableFrom(DatabaseViewModel::class.java)) {
                @Suppress("UNCHECKED_CAST")
                return DatabaseViewModel(app) as T
            }
            throw IllegalArgumentException("Unable to construct viewmodel")
        }
    }

    /**
     * This is the job for all coroutines started by this ViewModel.
     *
     * Cancelling this job will cancel all coroutines started by this ViewModel.
     */
    private val viewModelJob = SupervisorJob()

    /**
     * This is the main scope for all coroutines launched by MainViewModel.
     *
     * Since we pass viewModelJob, you can cancel all coroutines launched by uiScope by calling
     * viewModelJob.cancel()
     */
    private val viewModelScope = CoroutineScope(viewModelJob + Dispatchers.Main)

    /**
     * The data source this ViewModel will fetch results from.
     */
    private val databaseRepository = DatabaseRepository()


    /**
     * A playlist of videos that can be shown on the screen. Views should use this to get access
     * to the data.
     */

    val historyAll: LiveData<List<HistoryEntity>>
        get() = databaseRepository.historyAll

    val databaseInsetSuccess: LiveData<Boolean>
        get() = databaseRepository.databaseInsetSuccess

    val databaseRemoveSuccess: LiveData<Boolean>
        get() = databaseRepository.databaseRemoveSuccess


    /**
     * Refresh data from the repository. Use a coroutine launch to run in a
     * background thread.
     */
    fun setHistory(deepLink: String) {
        viewModelScope.launch {
            databaseRepository.setReferCode(deepLink)
        }
    }


    fun getAllHistory() {
        viewModelScope.launch {
            databaseRepository.getAllHistory()
        }
    }

    fun removeDatabase() {
        viewModelScope.launch {
            databaseRepository.removeDatabase()
        }
    }

    fun removeHistory(id: Int) {
        viewModelScope.launch {
            databaseRepository.removeHistory(id)
        }
    }


}
package com.jongzazaal.deeplinktester.di

import com.jongzazaal.deeplinktester.manager.database.DatabaseManager
import com.jongzazaal.deeplinktester.repository.DatabaseRepository
import com.jongzazaal.deeplinktester.viewmodel.DatabaseViewModel
import org.koin.android.ext.koin.androidApplication
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val appModule = module {

    factory{ DatabaseRepository() }

    //Database
    single<DatabaseManager>{ DatabaseManager }

    // ViewModel
    viewModel { DatabaseViewModel(androidApplication()) }

}

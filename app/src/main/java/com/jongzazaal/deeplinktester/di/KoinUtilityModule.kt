package com.jongzazaal.deeplinktester.di

import com.jongzazaal.core.DI.KoinContext
import org.koin.dsl.module

val utilityModule = module {
    single { KoinContext(get()) }
}
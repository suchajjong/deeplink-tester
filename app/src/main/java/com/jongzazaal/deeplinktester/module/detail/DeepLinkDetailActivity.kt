package com.jongzazaal.deeplinktester.module.detail

import android.os.Bundle
import android.os.Handler
import android.view.Menu
import android.view.MenuItem
import android.webkit.URLUtil
import android.widget.ImageView
import android.widget.TextView
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.SimpleItemAnimator
import com.google.android.ads.mediationtestsuite.MediationTestSuite
import com.google.android.gms.ads.*
import com.google.android.gms.ads.nativead.MediaView
import com.google.android.gms.ads.nativead.NativeAd
import com.google.android.gms.ads.nativead.NativeAdOptions
import com.google.android.gms.ads.nativead.NativeAdView
import com.jongzazaal.core.base.BaseActivity
import com.jongzazaal.core.base.BaseCommon
import com.jongzazaal.core.base.SpacesItemDecoration
import com.jongzazaal.core.extension.copyClipboard
import com.jongzazaal.core.extension.dp
import com.jongzazaal.core.extension.showToast
import com.jongzazaal.core.utility.AdMobUtility
import com.jongzazaal.core.utility.OpenExternalUtility
import com.jongzazaal.core.utility.ScreenUtility
import com.jongzazaal.deeplinktester.R
import com.jongzazaal.deeplinktester.databinding.ActivityDeepLinkDetailBinding
import com.jongzazaal.deeplinktester.module.detail.adapter.HistoryAdapter
import com.jongzazaal.deeplinktester.module.webview.WebViewActivity
import com.jongzazaal.deeplinktester.viewmodel.DatabaseViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel


class DeepLinkDetailActivity : BaseActivity(), BaseCommon {
    private val binding: ActivityDeepLinkDetailBinding by lazy {
        ActivityDeepLinkDetailBinding.inflate(layoutInflater)
    }
    val databaseViewModel: DatabaseViewModel by viewModel()

    private var adapter: HistoryAdapter? = null
    private var initialLayoutComplete = false
    private var initialLayoutComplete2 = false


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
//        binding.deepLink.setText("https://staging-www.punpro.com/a/6274b4e4d4d5560c2c081eb2")
//        binding.deepLink.setText("https://staging-www.punpro.com")
//        binding.deepLink.setText("punpro-uat://a/62a93c1f64371d092f038a83")

        initView()
        initListener()
        initViewModel()
        firstCreate()
    }

    override fun firstCreate() {
        databaseViewModel.getAllHistory()

    }

    override fun initView() {
        setAdapter()
//        binding.adView.setAdSize(AdSize.BANNER)
//        binding.adView.adUnitId = "ca-app-pub-3940256099942544/6300978111"
//        val adRequest = AdRequest.Builder().build()
//        binding.adView.loadAd(adRequest)

//        val ad = AdView(this)
//        binding.adViewContainer.addView(ad)
//        binding.adViewContainer.viewTreeObserver.addOnGlobalLayoutListener {
//            if (!initialLayoutComplete) {
//                initialLayoutComplete = true
//                ad.adUnitId = getString(R.string.banner_home_bottom)
//                ad.setAdSize(AdMobUtility.getAdSizeAdaptiveBanner(binding.adViewContainer))
//                ad.loadAd(adRequest)
//            }
//
//        }
        Handler().postDelayed({
            val adRequest = AdRequest.Builder().build()
            binding.adView.loadAd(adRequest)
//            val ad = AdView(this)
//            binding.adViewContainer.addView(ad)
//            binding.adViewContainer.viewTreeObserver.addOnGlobalLayoutListener {
//                if (!initialLayoutComplete) {
//                    initialLayoutComplete = true
//                    ad.adUnitId = getString(R.string.banner_home_bottom)
//                    ad.setAdSize(AdMobUtility.getAdSizeAdaptiveBanner(binding.adViewContainer))
//                    ad.loadAd(adRequest)
//                }
//
//            }
        }, 3000)
//        setNativeAds()

        /**
         * ads inline
         */
//        val adInline = AdView(this)
//        binding.adViewInLineContainer.addView(adInline)
//        binding.adViewContainer.viewTreeObserver.addOnGlobalLayoutListener {
//            if (!initialLayoutComplete2) {
//                initialLayoutComplete2 = true
//                adInline.adUnitId = getString(R.string.banner_home_bottom)
//                adInline.setAdSize(AdMobUtility.getAdSizeInlineBanner(binding.adViewContainer))
//                adInline.loadAd(adRequest)
//            }
//        }
    }

    private val adSize: AdSize
        get() {

            var adWidthPixels = binding.adViewContainer.width.toFloat()

//             If the ad hasn't been laid out, default to the full screen width.
            if (adWidthPixels == 0f) {
                adWidthPixels = ScreenUtility.getDisplay().widthPixels.toFloat()
            }

            val density = resources.displayMetrics.density
            val adWidth = (adWidthPixels / density).toInt()

            return AdSize.getCurrentOrientationAnchoredAdaptiveBannerAdSize(this, adWidth)
        }

    private fun setNativeAds(){
        val adLoader = AdLoader.Builder(this, "ca-app-pub-3940256099942544/1044960115")
            .forNativeAd { ad : NativeAd ->
                // Show the ad.
                // Assumes that your ad layout is in a file call native_ad_layout.xml
                // in the res/layout folder
                val adView = layoutInflater
                    .inflate(R.layout.adapter_history_ads, null) as NativeAdView
                // This method sets the text, images and the native ad, etc into the ad
                // view.
//                populateNativeAdView(nativeAd, adView)

                // Locate the view that will hold the headline, set its text, and use the
                // NativeAdView's headlineView property to register it.
                val headlineView = adView.findViewById<TextView>(R.id.ad_headline)
                headlineView.text = ad.headline
                adView.headlineView = headlineView

                // Locate the view that will hold the headline, set its text, and use the
                // NativeAdView's headlineView property to register it.
                val imgView = adView.findViewById<ImageView>(R.id.ad_app_icon)
                imgView.setImageDrawable(ad.icon?.drawable)
                adView.iconView = imgView

                val media = adView.findViewById<MediaView>(R.id.ad_media)
                media.mediaContent = ad.mediaContent
//                media.setOnHierarchyChangeListener(object :
//                    ViewGroup.OnHierarchyChangeListener {
//                    override fun onChildViewAdded(parent: View, child: View) {
//                        if (child is ImageView) {
//                            child.adjustViewBounds = true
//                        }
//                    }
//
//                    override fun onChildViewRemoved(parent: View, child: View) {}
//                })
                adView.mediaView = media

                // Call the NativeAdView's setNativeAd method to register the
                // NativeAdObject.
                adView.setNativeAd(ad)

                // Assumes you have a placeholder FrameLayout in your View layout
                // (with id ad_frame) where the ad is to be placed.
                binding.adViewContainer.removeAllViews()
                binding.adViewContainer.addView(adView)
            }
            .withAdListener(object : AdListener() {
                override fun onAdFailedToLoad(adError: LoadAdError) {
                    // Handle the failure by logging, altering the UI, and so on.
                    adError.message.showToast()
                }
            })
            .withAdListener(object : AdListener(){
                override fun onAdClicked() {
                    super.onAdClicked()
                }
            })
            .withNativeAdOptions(
                NativeAdOptions.Builder()
                // Methods in the NativeAdOptions.Builder class can be
                // used here to specify individual options settings.
                .build())
            .build()

        adLoader.loadAd(AdRequest.Builder().build())

    }

    override fun onResume() {
        super.onResume()
        loadAd(getString(R.string.ad_between_page))
    }
    override fun initListener() {
        binding.deepLinkBtn.setOnClickListener {
//            showInterstitial {
                val url = binding.deepLink.text.toString()
                if (url.isEmpty()){return@setOnClickListener}
                OpenExternalUtility.openDeepLink(this, binding.deepLink.text.toString())
                databaseViewModel.setHistory(url)
//            MobileAds.openAdInspector(this) {
//                // Error will be non-null if ad inspector closed due to an error.
//            }
//            MediationTestSuite.launch(this@DeepLinkDetailActivity)
//            }



        }
        binding.webViewBtn.setOnClickListener {
            val url = binding.deepLink.text.toString()
            if (URLUtil.isValidUrl(url)){
                WebViewActivity.start(this, url)
                databaseViewModel.setHistory(url)
            }
            else{
                "url not valid".showToast()
            }
        }
        binding.chromeCustomTabBtn.setOnClickListener {
            val url = binding.deepLink.text.toString()
            if (URLUtil.isValidUrl(url)){
                OpenExternalUtility.openChromeCustomTab(this, url)
                databaseViewModel.setHistory(url)
            }
            else{
                "url not valid".showToast()
            }
        }

        binding.clear.setOnClickListener {
            databaseViewModel.removeDatabase()
        }
    }

    override fun initViewModel() {
        databaseViewModel.historyAll.observe(this, Observer { result ->
            adapter?.refreshList(result)
        })

        databaseViewModel.databaseInsetSuccess.observe(this, Observer { result ->
            databaseViewModel.getAllHistory()
        })

        databaseViewModel.databaseRemoveSuccess.observe(this, Observer { result ->
            databaseViewModel.getAllHistory()
        })
    }

    override fun showLoading() {}

    override fun hideLoading() {}

    private fun setAdapter(){
        if (adapter == null){
            adapter = HistoryAdapter(arrayListOf())
            adapter!!.setClickListener { model ->
                binding.deepLink.setText(model.deepLink)
            }
            adapter!!.setClickCopyListener { model ->
                model.deepLink.copyClipboard(this)
            }
            adapter!!.setClickDeleteListener { model ->
                databaseViewModel.removeHistory(model.id)
            }
        }

        (binding.recycleView.itemAnimator as SimpleItemAnimator).supportsChangeAnimations = false
        binding.recycleView.adapter = adapter
        binding.recycleView.addItemDecoration(SpacesItemDecoration(4.dp))

    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.main_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_settings -> {
                OpenExternalUtility.openChromeCustomTab(this, "https://gitlab.com/suchajjong/deeplink-tester/-/wikis/นโยบายความเป็นส่วนตัว")
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

}
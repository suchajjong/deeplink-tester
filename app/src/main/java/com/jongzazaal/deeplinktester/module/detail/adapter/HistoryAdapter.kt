package com.jongzazaal.deeplinktester.module.detail.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.jongzazaal.deeplinktester.databinding.AdapterHistoryBinding
import com.jongzazaal.deeplinktester.manager.database.entity.HistoryEntity

class HistoryAdapter(
    private var data : MutableList<HistoryEntity>
): RecyclerView.Adapter<HistoryAdapter.ViewHolder>() {

    private var eventClickItemListener : ((model : HistoryEntity) -> Unit)? = null
    fun setClickListener(event : ((model : HistoryEntity) -> Unit)){
        eventClickItemListener = event
    }

    private var eventClickCopyItemListener : ((model : HistoryEntity) -> Unit)? = null
    fun setClickCopyListener(event : ((model : HistoryEntity) -> Unit)){
        eventClickCopyItemListener = event
    }

    private var eventClickDeleteItemListener : ((model : HistoryEntity) -> Unit)? = null
    fun setClickDeleteListener(event : ((model : HistoryEntity) -> Unit)){
        eventClickDeleteItemListener = event
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val viewHolder = AdapterHistoryBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(viewHolder)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(data[position])
    }

    override fun getItemCount(): Int {
        return  data.size
    }

    fun refreshList(listItem: List<HistoryEntity>){
        this.data = listItem.toMutableList()
        notifyDataSetChanged()
    }

    inner class ViewHolder(private val binding: AdapterHistoryBinding): RecyclerView.ViewHolder(binding.root){

        val context = binding.root.context
        fun bind(model: HistoryEntity){
            binding.layout.setOnClickListener { eventClickItemListener?.invoke(model) }
            binding.copy.setOnClickListener { eventClickCopyItemListener?.invoke(model) }
            binding.delete.setOnClickListener { eventClickDeleteItemListener?.invoke(model) }

            binding.deepLink.text = model.deepLink

        }
    }

}
package com.jongzazaal.deeplinktester.module.webview

import android.R
import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.net.Uri
import android.os.Bundle
import android.view.MenuItem
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.activity.OnBackPressedCallback
import androidx.appcompat.app.ActionBar
import com.jongzazaal.core.base.BaseActivity
import com.jongzazaal.core.extension.showToast
import com.jongzazaal.deeplinktester.databinding.ActivityWebViewBinding


class WebViewActivity : BaseActivity() {
    private var url: String = ""
    private val binding: ActivityWebViewBinding by lazy {
        ActivityWebViewBinding.inflate(layoutInflater)
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        onBackPressedDispatcher.addCallback(this, onBackPressedCallback)
        addBackButton()
        url = intent.getStringExtra(URL_PAGE)?:""
        setUpWebviewSettings()
        openUrl(url)
    }
    private val onBackPressedCallback = object: OnBackPressedCallback(true) {
        override fun handleOnBackPressed() {
            onBackPressedCustom()
        }
    }

    @SuppressLint("SetJavaScriptEnabled")
    private fun setUpWebviewSettings() {
        binding.webView.setBackgroundColor(Color.TRANSPARENT)
        binding.webView.settings.javaScriptEnabled = true
        binding.webView.settings.domStorageEnabled = true
        binding.webView.settings.allowFileAccessFromFileURLs = true
        binding.webView.settings.allowUniversalAccessFromFileURLs = true
        binding.webView.settings.allowFileAccess = true
        binding.webView.setOnLongClickListener { return@setOnLongClickListener true }
        binding.webView.webViewClient = object : WebViewClient() {

            override fun shouldOverrideUrlLoading(view: WebView?, url: String?): Boolean {
                try {
                    this@WebViewActivity.startActivity(
                        Intent(Intent.ACTION_VIEW).apply {
                            data = Uri.parse(url)
                        }
                    )
                    return true
                } catch (exception: Exception) {
                    exception.localizedMessage.showToast()
                }
                return false
            }

        }
    }
    private fun openUrl(url: String){
        binding.webView.loadUrl(url)
    }

    private fun onBackPressedCustom() {
        if (binding.webView.canGoBack()){
            binding.webView.goBack()
        }
        else{
            onBackPressedCallback.isEnabled = false
            onBackPressedDispatcher.onBackPressed()
        }

    }

    private fun addBackButton(){
        val actionBar: ActionBar? = supportActionBar
        actionBar?.setDisplayHomeAsUpEnabled(true)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.home -> {
                finish()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    companion object {
        private const val URL_PAGE = "url"


        fun start(context: Context, url:String) {
            context.startActivity(Intent(context, WebViewActivity::class.java).apply {
                putExtra(URL_PAGE, url)
            })
        }

    }

}
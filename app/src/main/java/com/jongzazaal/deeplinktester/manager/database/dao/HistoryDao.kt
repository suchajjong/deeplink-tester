package com.jongzazaal.deeplinktester.manager.database.dao

import androidx.room.*
import com.jongzazaal.deeplinktester.manager.database.entity.HistoryEntity

@Dao
interface HistoryDao {

    @Query("SELECT * FROM HistoryEntity ORDER BY last_modified_time DESC")
    fun getAllHistory(): List<HistoryEntity>?

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(vararg refer: HistoryEntity)

    @Delete
    fun delete(user: HistoryEntity)

    @Query("DELETE FROM HistoryEntity WHERE id=:id")
    fun deleteId(id:Int)

    @Query("DELETE FROM HistoryEntity")
    fun deleteAll()
}
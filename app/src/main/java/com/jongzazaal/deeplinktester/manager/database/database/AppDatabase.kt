package com.jongzazaal.deeplinktester.manager.database.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.jongzazaal.deeplinktester.manager.database.dao.HistoryDao
import com.jongzazaal.deeplinktester.manager.database.entity.HistoryEntity

@Database(entities = arrayOf(HistoryEntity::class), version = 1)
abstract class AppDatabase : RoomDatabase() {
    abstract fun historyDao(): HistoryDao
}
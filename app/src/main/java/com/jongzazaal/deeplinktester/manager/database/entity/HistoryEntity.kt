package com.jongzazaal.deeplinktester.manager.database.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class HistoryEntity(
    @PrimaryKey(autoGenerate = true) @ColumnInfo(name = "id") val id: Int,
    @ColumnInfo(name = "deep_link") val deepLink: String,
    @ColumnInfo(name = "last_modified_time") var last_modified_time: Long?

)
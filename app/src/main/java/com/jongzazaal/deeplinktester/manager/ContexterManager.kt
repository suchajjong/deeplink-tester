package com.jongzazaal.deeplinktester.manager

import android.annotation.SuppressLint
import android.app.Application
import android.content.Context
import androidx.annotation.StringRes

class ContexterManager private constructor() {

    private lateinit var context: Context
    private lateinit var application: Application

    companion object {
        @SuppressLint("StaticFieldLeak")
        private val contextInstance = ContexterManager()

        fun getInstance(): ContexterManager {
            return contextInstance
        }
    }

    fun setApplicationContext(context: Context) {
        this.context = context
    }

    fun getApplicationContext(): Context {
        return context
    }

    fun getApplication(): Application {
        return application
    }

    fun setApplication(application: Application){
        this.application = application
    }

    fun getStirngByResource(@StringRes res: Int) = context.getString(res)

}
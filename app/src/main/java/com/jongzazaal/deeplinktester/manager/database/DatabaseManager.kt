package com.jongzazaal.deeplinktester.manager.database

import androidx.room.Room
import com.jongzazaal.core.common.AppKey
import com.jongzazaal.deeplinktester.manager.ContexterManager
import com.jongzazaal.deeplinktester.manager.database.database.AppDatabase
import com.jongzazaal.deeplinktester.manager.database.entity.HistoryEntity
import java.util.*

object DatabaseManager {
    private var databaseApp: AppDatabase? = null

    fun getDatabase():AppDatabase{
        if (databaseApp == null){
            databaseApp = Room.databaseBuilder(
                ContexterManager.getInstance().getApplicationContext(),
                AppDatabase::class.java, AppKey.DB_NAME_HISTORY
            ).fallbackToDestructiveMigration().build()
        }
        return databaseApp!!
    }

    fun setHistory(deepLink: String){
        val model = HistoryEntity(
            id = 0,
            deepLink = deepLink,
            last_modified_time = Date().time
        )
        getDatabase().historyDao().insert(model)
    }

    fun removeDatabase(){
        getDatabase().historyDao().deleteAll()
    }

    fun removeHistory(id: Int?){
        if (id != null){
            getDatabase().historyDao().deleteId(id)
        }
    }

    fun getAllHistory(): List<HistoryEntity>?{
        return getDatabase().historyDao().getAllHistory()

    }
}